package com.jianli.clickhouse;

import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.settings.ClickHouseProperties;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ClickHouseApp {


  public static void main(String[] args) {

    ClickHouseProperties properties = new ClickHouseProperties();
    ClickHouseDataSource dataSource =
        new ClickHouseDataSource("jdbc:clickhouse://localhost:8123", properties);
    try {
      Connection connection = dataSource.getConnection();
      connection.createStatement().execute("CREATE DATABASE IF NOT EXISTS test");
      connection.createStatement().execute("DROP TABLE IF EXISTS test.logs");
      connection.createStatement().execute(
          "CREATE TABLE IF NOT EXISTS test.logs (userId Int32, metrics String) ENGINE = TinyLog");

      PreparedStatement statement =
          connection
              .prepareStatement("INSERT INTO test.logs (metrics, userId) VALUES (?, ?)");

      statement.setString(1, "test");
      statement.setInt(2, 1);
      statement.addBatch();

      statement.setString(1, "test2");
      statement.setInt(2, 2);
      statement.addBatch();

      statement.executeBatch();

      ResultSet rs =
          connection.createStatement()
              .executeQuery("SELECT * from test.logs");
      rs.next();

      System.out.println(rs);

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


  }

}
